
// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log(daftarHewan);

// soal 2
var data = {
    name : "John" ,
    age : 30 ,
    address : "Jalan Pelesiran" ,
    hobby : "Gaming" 
}

function introduce(bio) {
    return `Nama saya ${bio.name}, umur saya ${bio.age} tahun, alamat saya di ${bio.address}, dan saya punya hobby yaitu ${bio.hobby}`;
}
var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3
function hitung_huruf_vokal(kata) {
    let jumlah = 0;
    // kata.forEach(huruf => {
    //     if (huruf === 'a' || huruf === 'i' || huruf === 'u' || huruf === 'e' || huruf === 'o') {
    //         jumlah++;
    //     }
    // });
    for (let i = 0; i < kata.length; i++) {
        if (kata[i] === 'a' || kata[i] === 'i' || kata[i] === 'u' || kata[i] === 'e' || kata[i] === 'o' || kata[i] === 'A' || kata[i] === 'I' || kata[i] === 'U' || kata[i] === 'E' || kata[i] === 'O') {
            jumlah++;
        }
        
    }
    return jumlah;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2

// soal 4
function hitung(number) {
    let arr = [];

    if ((number-1) >= 0) {
        for (let i = 0; i < 100; i++) {
            if (i % 2 === 0) {
                arr.push(i);
            }
            
        }
    
        return arr[number-1];    

    } else if ((number-1) < 0) {

        for (let i = 1; i > -100; i--) {
            if (i % -2 === 0) {
                arr.push(i);
            }
            
        }
    
        return arr[number + 1];  
    } 
    
    
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8